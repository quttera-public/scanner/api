#!/usr/bin/php
<?php 

/*
REST API
--------------------------------------------------------------------------------
POST /api/v3/<api-key>/url/scan/<domain-name>[.json|.xml|.yaml]          Submits domain for new scan
POST /api/v3/<api-key>/url/scan/<base64-encoded-url>[.json|.xml|.yaml]   Submits domain for new scan
curl -X POST http://api.quttera.com/api/v3/A58E3D9E-514B-31DE-9617-2EB82B6550D0/url/scan/quttera.com.xml
--------------------------------------------------------------------------------
PUT /api/v3/<api-key>/url/scan/<domain-name>[.json|.xml|.yaml]           Resubmits domain, with existing threat report, for re-scan
PUT /api/v3/<api-key>/url/scan/<base64-encoded-url>[.json|.xml|.yaml]    Resubmits domain, with existing threat report, for re-scan
curl -X PUT -H 'Content-Length: 0' http://api.quttera.com/api/v3/A58E3D9E-514B-31DE-9617-2EB82B6550D0/url/scan/quttera.com.yaml
--------------------------------------------------------------------------------
GET /api/v3/<api-key>/url/status/<domain-name>[.json|.xml|.yaml]         Returns current status of domain submitted for scan
GET /api/v3/<api-key>/url/status/<base64-encoded-url>[.json|.xml|.yaml]  Returns current status of domain submitted for scan
curl http://api.quttera.com/api/v3/A58E3D9E-514B-31DE-9617-2EB82B6550D0/url/status/quttera.com.yaml
--------------------------------------------------------------------------------
GET /api/v3/<api-key>/url/report/<domain-name>[.json|.xml|.yaml]         Returns malware scan report for domain
GET /api/v3/<api-key>/url/report/<base64-encoded-url>[.json|.xml|.yaml]  Returns malware scan report for domain
curl http://api.quttera.com/api/v3/A58E3D9E-514B-31DE-9617-2EB82B6550D0/url/report/quttera.com.yaml
--------------------------------------------------------------------------------
GET /api/v3/<api-key>/blacklist/status/<domain-name>[.json|.xml|.yaml]         Returns current blacklist status of the domain
GET /api/v3/<api-key>/blacklist/status/<base64-encoded-url>[.json|.xml|.yaml]  Returns current blacklist status of the domain
curl http://api.quttera.com/api/v3/A58E3D9E-514B-31DE-9617-2EB82B6550D0/blacklist/status/quttera.com.yaml
*/

    date_default_timezone_set("Europe/Berlin");

    /**
     * @brief       usage - how to invoke this tool 
     * @param[in]   $argc - number of script input arguments
     * @param[in]   $argv - script input arguments array
     */
    function usage($argc,$argv) 
    {
        printf("PHP example how to use Quttera REST APIv3\n");
        printf("%s <api-server> <api-key> <domain/url>\n",$argv[0]);
    }

    
    /**
     * @brief       send's HTTP POST request to provided URL 
     * @param[in]   $url - URL to send POST request
     * @return      return decoded response
     */
    function post($url)
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_POST, 1);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Length: ' . "0" ));
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($curl,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($curl);
        printf( "Curl status: %s\n", curl_error($curl) );
        curl_close ($curl);
        
        if( $response )
        {
            return json_decode($response,true);
        }
        return null;
    }


    /**
     * @brief       send's HTTP GET request to provided URL 
     * @param[in]   $url - URL to send the request
     * @return      return decoded response
     */
    function get($url)
    {
        $curl = curl_init();
        curl_setopt($curl,CURLOPT_URL,$url);
        curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($curl,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($curl);
        curl_close ($curl);
        if( $response )
        {
            return json_decode($response,true);
        }
        return null;
    }



    /**
     * @brief       print's investigation status returned by API query 
     * @param[in]   $status - API investigation status to print
     */
    function print_status($status)
    {
        $date_str = date('Y-m-d H:i:s', $status["time"]);
        printf( "%s URL: %s | blacklisted: %s | oper. status %s | scanner result: %s\n", 
		$date_str,
		$status["url"],
		$status["blacklisted"],
		$status["state"], 
		$status["scanner_result"]);
    }


    /**
     * @brief       prints blacklisting status returned by API query
     * @param       status - blacklisting status to print
     */
    function print_blacklist_status($status)
    {
        //var_dump($status);
		printf(	"\r\n\r\n%s blacklisting status %s\r\n\r\n", 
				$status["domain"], 
				$status["status"]);
    }

    /**
     * @brief       print investigation report retrieved from PI
     * @param[in]   report - investigation report to print
     */
    function print_report($report)
    {
        printf("Scanner state: %10s\n",     $report["state"]);
        printf("Report time: %10s\n",       $report["timestr"]);
        printf("Scanned bytes: %10d\n",     $report["scanned_content"]);
        printf("Scanned files: %d\n",       $report["scanned_files"]);
        printf("Detected PS files: %d\n",   $report["psuspicious_files"]);
        printf("Detected S files: %d\n",    $report["suspicious_files"]);
        printf("Detected M files: %d\n",    $report["malicious_files"]);
        printf("Detected domains: %d\n",    $report["domains_count"]);
        printf("Detected iframes: %d\n",    $report["iframes_count"]);
        printf("Detected links: %d\n",      $report["links_count"]);

        printf("Detected blacklisted domains: %d\n",$report["blacklisted_domains_count"]);
        printf("Detected blacklisted iframes: %d\n",$report["blacklisted_iframes_count"]);
        printf("Detected blacklisted links: %d\n",  $report["blacklisted_links_count"]);
        printf("Blacklisting status: %s\n",         $report["blacklist_report"]["blacklist_status"]);


        if( $report["domains_count"] > 0 )
        {
            printf("\nDetected domain names:\n");
            foreach(array_keys($report["domains"]) as $key){
                printf("%s\t%s\n",$key,$report["domains"][$key]);
            }
        }

        if( $report["links_count"] > 0 )
        {
            printf("\nExtracted links:\n");
            foreach(array_keys($report["links"]) as $key){
                printf("%s\t%s\n",$key,$report["links"][$key]);
            }
        }


        if( $report["iframes_count"] > 0 )
        {
            printf("\nExtracted iframes:\n");
            foreach(array_keys($report["iframes"]) as $key){
                printf("%s\t%s\n",$key,$report["iframes"][$key]);
            }
        }


        if( $report["blacklisted_domains_count"] > 0 )
        {
            printf("\nDetected blacklisted domains:\n");
            foreach(array_keys($report["blacklisted_domains"]) as $key){
                printf("%s\t%s\n",$key,$report["blacklisted_domains"][$key]);
            }
        }


        if( $report["blacklisted_links_count"] > 0 )
        {
            printf("\nDetected blacklisted links:\n");
            foreach(array_keys($report["blacklisted_links"]) as $key){
                printf("%s\t%s\n",$key,$report["blacklisted_links"][$key]);
            }
        }


        if( $report["blacklisted_iframes_count"] > 0 )
        {
            printf("\nDetected blacklisted iframes:\n");
            foreach(array_keys($report["blacklisted_iframes"]) as $key){
                printf("%s\t%s\n",$key,$report["blacklisted_iframes"][$key]);
            }
        }

        printf("\nBlacklisting report:\n");
        foreach($report["blacklist_report"]["providers"] as $provider){
            printf("%20s:\t\t %s epoc[%d]\n",$provider["name"],$provider["status"],$provider["time"]);
        }

        printf("\nScanned files:\n");
        foreach($report["files"] as $file){
            printf("\n\n");
            foreach(array_keys($file) as $key){
                printf("%10s\t%s\n", $key, $file[$key]);
            }
        }
    }


    /**
     * @brief       submit domain investigation request 
     * @param[in]   $api_server - API server to send request
     * @param[in]   $api_key    - API key to use for request
     * @param[in]   $url        - URL/domain name to scan
     * @return      returns response from the API server
     */
    function start_scan($api_server,$api_key,$url)
    {
        $scan_request = "http://".$api_server . "/api/v3/" . $api_key . "/url/scan/" . $url;
        printf("Issue scan request with %s\n",$scan_request);
        return post($scan_request);
    }


    /**
     * @brief   retrieve current investigation status of a provided URL 
     * @param[in]   $api_server - API server to send request
     * @param[in]   $api_key    - API key to use for request
     * @param[in]   $url        - URL/domain name to retrieve investigation status
     * @return      returns response from the API server    
     */
    function get_scan_progress($api_server,$api_key,$url)
    {
        $status_request = "http://".$api_server . "/api/v3/" . $api_key . "/url/status/" . $url;
        $rc = get($status_request );
        //var_dump($rc);
        if($rc == null ){
            printf("Call to $status_request failed\n");
            return $rc;
        }

        return $rc;
    }



    /**
     * @brief   retrieve investigation report of the provided URL/domain name
     * @param[in]   $api_server - API server to send request
     * @param[in]   $api_key    - API key to use for request
     * @param[in]   $url        - URL/domain name to scan
     * @return      returns response from the API server
     */
    function get_scan_report($api_server,$api_key,$url)
    {
        $report_request = "http://".$api_server . "/api/v3/" . $api_key . "/url/report/" . $url;
   
        $rc = get($report_request );

        if($rc == null ){
            printf("Call to $report_request failed\n");
            return $rc;
        }

        return $rc;
    }

    /**
     * @brief   retrieve current blacklisting status of a provided URL 
     * @param[in]   $api_server - API server to send request
     * @param[in]   $api_key    - API key to use for request
     * @param[in]   $url        - URL/domain name to retrieve investigation status
     * @return      returns response from the API server    
     */
    function get_blacklist_status($api_server,$api_key,$url)
    {
        $status_request = "http://".$api_server . "/api/v3/" . $api_key . "/blacklist/status/" . $url;
        $rc = get($status_request );
        //var_dump($rc);
        if($rc == null )
        {
            printf("Call to $status_request failed\n");
            return $rc;
        }
        return $rc;
    }



    /*
     * entry point
     */

    if( $argc < 4 )
    {
        usage($argc,$argv);
        exit(-1);
    }

    printf("Getting blacklisting status of %s\n",$argv[3]);

    $bl_status = get_blacklist_status($argv[1],$argv[2],$argv[3]);

    print_blacklist_status($bl_status);

    printf("Starting investigation of %s on API server %s with API key %s\n",$argv[3],$argv[1],$argv[2]);
    /*
     * send investigation request to provided API server
     */
    $reply = start_scan($argv[1],$argv[2],$argv[3]);
    if ( $reply == null )
    {
        printf("Failed to submit request to Quttera API server.\n");
        exit(-1);
    }


    if( $reply["error"] != 200 ){
        printf("Error, failed to communicate with API server: %s. Error: %d\n",$argv[1],$reply["error"]);
        exit(-1);
    }

    $status = null;

    /* 
     * if this domain already scanned and send reply contains "status" fields 
     */
    if( array_key_exists( "status", $reply ) )
    { 
        $status = $reply["status"];
        print_status($status);
    }
    
    /*
     * This loop runs until server finish URL/domain investigation
     */
    while( $status == null or $reply["error"] == 200 )
    {   
        /* sleep 0.5 second */
        usleep(500000);
        $reply  = get_scan_progress($argv[1],$argv[2],$argv[3]);
        $status = $reply["status"];
        print_status($status);
        if( $status["state"] == "DONE" )
        {
            /* 
             * investigation is done, we can query for investigation report 
             */
            break;
        }
    }

    $reply = get_scan_report($argv[1],$argv[2],$argv[3]);
    $report = $reply["report"];
    print_report($report); 
?>
